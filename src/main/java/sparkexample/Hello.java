package sparkexample;

import static spark.Spark.*;

public class Hello {
    public static void main(String[] args) {
        get("/mo", (request, response) -> {
            String to = request.queryParams("to");
	    String from = request.queryParams("from");
            String msg = request.queryParams("text");
            return "MO: " + msg;
        });

        post("/mo", (request, response) -> {
            String to = request.queryParams("to");
            String from = request.queryParams("from");
            String msg = request.queryParams("text");
            return "MO: " + msg;
        });

    }

}
