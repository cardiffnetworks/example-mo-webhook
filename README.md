# example-mo-webhook

Just a basic (no frills) example of how to read POST and GET params as passed by the SMS Gateway


# Quick Start

### Get source files from repo

```
git clone git@gitlab.com:cardiffnetworks/example-mo-webhook.git
```

then

```
cd example-mo-webhook
```
 

### Build & run service with Docker

```
docker build -t example-mo-webhook:latest .

docker run -d --rm  --name exmple-mo -p 4567:4567 example-mo-webhook:latest

```

#OR

#### Build & run service with Docker Compose


```
 docker-compose down && docker-compose build && docker-compose up
 ```

#### Deploy to a Kubernetes cluster

Manifests not included but available on demand

```
kubectl apply -f infrastructure/kubernetes/
```

# Accessing the Service:

### GET
```
http://localhost:4567/mo?to=6562&from=250781xxxxxx&text=geopoll%20test
```

### POST
```
POST http://localhost:4567/mo HTTP/1.1
Content-Type: application/x-www-form-urlencoded

to=6562
&from=250781xxxxxx
&text=geopoll test
```





